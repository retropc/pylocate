#!/usr/bin/env python3
import os, sys, trie, platform, ctypes

MAX_RESULTS = 200
EDITOR = os.environ.get("PYLOCATE_EDITOR")

if platform.system() == "Windows":
  exc = lambda x: ctypes.windll.shell32.ShellExecuteW(0, u'open',  ctypes.c_wchar_p(x), None, None, 1)
  exc_editor = lambda x: ctypes.windll.shell32.ShellExecuteW(0, u'open', unicode(EDITOR), ctypes.c_wchar_p(x), None, 1)
  from indexworker_generic import IndexWorkerGeneric as IndexWorker
else:
  import signal
  SIGNAL_SET = False
  LAUNCHER = os.environ.get("PYLOCATE_LAUNCHER", "xdg-open")

  def pre_exec():
    global SIGNAL_SET
    if not SIGNAL_SET:
      SIGNAL_SET = True
      signal.signal(signal.SIGCHLD, signal.SIG_IGN)

  def exc(x):
    pre_exec()
    os.spawnvp(os.P_NOWAIT, LAUNCHER, [LAUNCHER, x])

  def exc_editor(x):
    pre_exec()
    os.spawnvp(os.P_NOWAIT, EDITOR, [EDITOR, x])

  from indexworker_unix import IndexWorkerUnix as IndexWorker
#from indexworker_generic import IndexWorkerGeneric as IndexWorker

SCRIPTPATH, _ = os.path.split(sys.argv[0])

class PyIndexGUI:
  def __init__(self, i, index_worker):
    self.__base = i.metadata["base"]
    self.__index_worker = index_worker
    self.__data = []

    builder = Gtk.Builder()
    builder.add_from_file(os.path.join(SCRIPTPATH, "gui.xml"))

    builder.connect_signals(self)

    self.__window = builder.get_object("window")
    self.__treeview = builder.get_object("results")
    self.__statusbar = builder.get_object("statusbar")
    self.__search = builder.get_object("search")
#    self.__spinner = builder.get_object("spinner")
    self.__tag = 0
    self.__last_tag_seen = -1
    self.__timer = False
    self.__setup_treeview()
    self.__set_spinner_active(False)

  def __setup_treeview(self):
    t = self.__treeview
    self.__listmodel = Gtk.ListStore(str, str)
    t.set_model(self.__listmodel)

    cell = Gtk.CellRendererText()
    col = Gtk.TreeViewColumn("Filename")
    col.pack_start(cell, True)
    col.set_attributes(cell, text=0)
    t.append_column(col)

  def show(self):
    self.__window.show()

  def on_window_destroy(self, widget, data=None):
    Gtk.main_quit()

  def on_window_key_press_event(self, widget, data=None):
    if data.keyval == Gdk.KEY_Escape:
      self.__window.destroy()

  def get_full_path(self, column):
    return os.path.join(self.__base, self.__data[column[0]])

  def on_results_row_activated(self, widget, column, data=None):
    exc(self.get_full_path(column))
    self.__window.destroy()

  def on_results_button_press_event(self, widget, event):
    if event.type != Gdk.EventType.BUTTON_PRESS or event.button not in (2, 3):
      return

    tree = self.__treeview.get_path_at_pos(int(event.x), int(event.y))
    if not tree:
      return

    p = self.get_full_path(tree[0])
    if event.button == 3:
      p = os.path.dirname(p)
      exc(p)
    if event.button == 2:
      exc_editor(p)
      self.__window.destroy()

  def set_statusbar(self, text):
    cid = self.__statusbar.get_context_id("Status bar")
    self.__statusbar.pop(cid)
    self.__statusbar.push(cid, text)

  def clear(self):
    self.__data = []
    self.__listmodel.clear()
    self.set_statusbar("")

  def on_results_cursor_changed(self, widget, data=None):
    _, row = self.__treeview.get_selection().get_selected_rows()
    if row:
      self.set_statusbar(self.get_full_path(row[0]))

  def __clear_if_no_results(self):
    self.__timer = False
    if self.__tag != self.__last_tag_seen:
      self.__last_tag_seen = self.__tag
      self.clear()

  def on_search_changed(self, widget, data=None):
    t = widget.get_text()

    self.__tag+=1
    if not self.__timer:
      self.__timer = True
      GLib.timeout_add(100, self.__clear_if_no_results)
    self.__set_spinner_active(True)
    self.__index_worker.search(t, self.__tag)

  def __set_spinner_active(self, value):
    if value:
      self.__search.override_background_color(Gtk.StateType.NORMAL, Gdk.RGBA(1, 1, 0, 0.2))
    else:
      self.__search.override_background_color(Gtk.StateType.NORMAL, Gdk.RGBA(0, 1, 0, 0.4))

  def add(self, data, tag, in_thread=False):
    if not in_thread:
      GLib.idle_add(lambda: self.add(data, tag, True))
      return

    if self.__tag != tag:
      return
    if self.__tag != self.__last_tag_seen:
      self.__last_tag_seen = self.__tag
      self.clear()

    if data == True:
      self.__set_spinner_active(False)
      return
    for x in data:
      self.__data.append(os.path.join(*x))
      self.__listmodel.append(x[::-1])
    self.__treeview.columns_autosize()

def alert(text):
  q = Gtk.MessageDialog(parent=None, destroy_with_parent=True, message_type=Gtk.MessageType.INFO, buttons=Gtk.ButtonsType.OK, text=text)
  q.run()
  q.destroy()

def main(indexfn):
  i = trie.FIndexReadTrie(indexfn)
  try:
    def callback(results, tag):
      window.add(results, tag)

    it = IndexWorker(i, callback, max_results=MAX_RESULTS)
    it.start()
    try:
      window = PyIndexGUI(i, it)
      window.show()
      Gtk.main()
    finally:
      it.terminate()
  finally:
    i.close()

if __name__ == "__main__":
  if sys.argv[1:2] == ["--child"]:
    import indexworker_unix
    indexworker_unix.child(sys.argv[2:])
  else:
    import gi
    gi.require_version("Gtk", "3.0")
    from gi.repository import Gtk, Gdk, GLib
    from gi.repository import Gdk as gdk

    if len(sys.argv) < 2:
      alert("usage: %s [index file]" % sys.argv[0])
    else:
      try:
        main(sys.argv[1])
      except KeyboardInterrupt:
        pass
