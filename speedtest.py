import indexer
import trie
import time
import os
import sys

def produce():
  f = open("dump", "w")
  i = 0
  idx = indexer.SerialIndexer("", [sys.argv[1]], )
  idx.start()
  for x in idx:
    f.write(x[0] + "\n")
    f.write(x[1] + "\n")
    i+=1
    if i > 175000:
      break

def run():
  f = open("dump", "r")
  a = []
  try:
    i = iter(f)
    while True:
      b = next(i)[:-1]
      c = next(i)[:-1]
      a.append((b, c))
  except StopIteration:
    pass

  newfile = "foo.new"
  i = trie.FIndexWriteTrie(newfile, {"base": ""})
  try:
    for root, filename in a:
      i.add(root, filename)
  finally:
    i.close()

def main():
  t = time.time()
  run()
  t2 = time.time()
  print(t2 - t)

if __name__ == "__main__":
  if not os.path.exists("dump"):
    produce()
  main()
